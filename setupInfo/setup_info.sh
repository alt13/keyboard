######################################
#     ___   _    _____ __   _____    #
#    / _ \ | |  |_   _/  | |____ |   #
#   / /_\ \| |    | | `| |     / /   #
#   |  _  || |    | |  | |     \ \   #
#   | | | || |____| | _| |_.___/ /   #
#   \_| |_/\_____/\_/ \___/\____/    #
#      https://gitlab.com/alt13      #
######################################
# setup guide  https://docs.qmk.fm/#/
# rules.mk     https://github.com/qmk/qmk_firmware/blob/master/docs/getting_started_make_guide.md

# based on     https://www.40percent.club/2019/06/semaphore.html

# http://www.keyboard-layout-editor.com/
# https://github.com/qmk/qmk_firmware/blob/master/docs/keycodes.md   - all keycodes



##########################
# Set up QMK environment #
##########################

# install QMK
paru -S qmk

# after installing set it up
qmk setup

# test by building a firmware
qmk flash -kb semaphore -km default
# OK "* The firmware size is fine - 26356/28672 (2316 bytes free)"




#####################
# Building Firmware #
#####################

# move "semaphore" to "qmk_firmware/keyboards"

# edit master side (if needed compile and flash for both sides)
qmk_firmware/keyboards/semaphore/config.h

# compile
# qmk compile -kb <keyboard> -km <keymap>
qmk compile -kb semaphore -km default
# build directory: qmk_firmware/.build
#   rules.mk - select folder to compile



##################
# Flash Firmware #
##################

# Flash Firmware
# For Arduino pro micro:
#   - hold reset > connect cable
#   - flash (limited time window)
# flash from qmk_firmware directory
#qmk flash -kb <my_keyboard> -km <my_keymap>
qmk flash -kb semaphore -km default

