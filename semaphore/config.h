#pragma once

/* key matrix size */
// Rows are doubled-up
#define MATRIX_ROWS 10
#define MATRIX_COLS 6

// wiring of each half
#define MATRIX_ROW_PINS { B2, B6, D4, C6, D7 }
#define MATRIX_COL_PINS { F4, F5, F6, F7, B1 ,B3 }
// #define MATRIX_COL_PINS { B3, B1, F7, F6, F5, F4} //uncomment this line and comment line above if you need to reverse left-to-right key order

#define DIODE_DIRECTION COL2ROW
#define SOFT_SERIAL_PIN D0

/* Mechanical locking support. Use KC_LCAP, KC_LNUM or KC_LSCR instead in keymap */
#define LOCKING_SUPPORT_ENABLE
/* Locking resynchronize hack */
#define LOCKING_RESYNC_ENABLE

/* Tap shorter than 200ms will be a tap, anything longer will be held (200 default) */
#define TAPPING_TERM 100

// Compile and flash for left/right side
#define MASTER_LEFT
// #define MASTER_RIGHT

