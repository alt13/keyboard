# Build Options
BOOTMAGIC_ENABLE = no 		# hold a key (usually Escape by default) to reset the EEPROM
MOUSEKEY_ENABLE = yes       # control over cursor movements and clicks via keycodes/custom functions
EXTRAKEY_ENABLE = yes       # use the system and audio control key codes
CONSOLE_ENABLE = yes        # print messages that can be read using hid_listen
COMMAND_ENABLE = yes        # Commands for debug and configuration
NKRO_ENABLE = yes           # allows the keyboard to tell the host OS that up to 248 keys are held down at once
BACKLIGHT_ENABLE = no       # Enable keyboard backlight functionality
AUDIO_ENABLE = no           # allows you output audio on the C6 pin
SPLIT_KEYBOARD = yes        # enables split keyboard support (dual MCU like the let's split and bakingpy's boards) and includes all necessary files located at quantum/split_common

