#include QMK_KEYBOARD_H

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

    // Main
    [0] = LAYOUT(
        KC_ESCAPE,               KC_1,         KC_2,         KC_3,         KC_4,              KC_5,                    KC_6,         KC_7,    KC_8,     KC_9,      KC_0,         KC_DELETE,
        KC_TAB,                  KC_Q,         KC_W,         KC_E,         KC_R,              KC_T,                    KC_Y,         KC_U,    KC_I,     KC_O,      KC_P,         KC_LEFT_BRACKET,
        KC_BACKSPACE,            KC_A,         KC_S,         KC_D,         KC_F,              KC_G,                    KC_H,         KC_J,    KC_K,     KC_L,      KC_SEMICOLON, KC_QUOT,
        SC_LSPO,                 KC_Z,         KC_X,         KC_C,         KC_V,              KC_B,                    KC_N,         KC_M,    KC_COMMA, KC_DOT,    KC_GRAVE,     SC_RSPC,
        LCTL_T(KC_LEFT_BRACKET), KC_BACKSLASH, KC_LALT,      KC_LEFT_GUI,  MO(2),             KC_SPACE,                KC_ENTER,     MO(1),   KC_RGUI,  KC_RALT,   KC_SLASH,     RCTL_T(KC_RIGHT_BRACKET)),

    // MO(1)
    [1] = LAYOUT(
        XXXXXXX,                 KC_F1,        KC_F2,        KC_F3,        KC_F4,             KC_F5,                   KC_F6,        KC_F7,   KC_F8,    KC_F9,     KC_F10,       KC_F11,
        XXXXXXX,                 XXXXXXX,      KC_MS_BTN1,   KC_MS_UP,     KC_MS_BTN2,        KC_MS_WH_UP,             KC_PGUP,      XXXXXXX, KC_UP,    XXXXXXX,   KC_HOME,      KC_F12,
        XXXXXXX,                 XXXXXXX,      KC_MS_LEFT,   KC_MS_DOWN,   KC_MS_RIGHT,       KC_MS_WH_DOWN,           KC_PAGE_DOWN, KC_LEFT, KC_DOWN,  KC_RIGHT,  KC_END,       KC_PRINT_SCREEN,
        XXXXXXX,                 XXXXXXX,      KC_MS_ACCEL0, KC_MS_ACCEL1, KC_MS_ACCEL2,      XXXXXXX,                 XXXXXXX,      XXXXXXX, XXXXXXX,  XXXXXXX,   XXXXXXX,      XXXXXXX,
        XXXXXXX,                 XXXXXXX,      XXXXXXX,      XXXXXXX,      _______,           XXXXXXX,                 XXXXXXX,      _______, XXXXXXX,  XXXXXXX,   XXXXXXX,      XXXXXXX),

    // MO(2)
    [2] = LAYOUT(
        QK_BOOTLOADER,           XXXXXXX,      XXXXXXX,      XXXXXXX,      XXXXXXX,           XXXXXXX,                 KC_NUM_LOCK,  XXXXXXX, XXXXXXX,  XXXXXXX,   XXXXXXX,      XXXXXXX,
        KC_TAB,                  XXXXXXX,      XXXXXXX,      XXXXXXX,      KC_AUDIO_VOL_UP,   KC_BRIGHTNESS_UP,        XXXXXXX,      KC_KP_7, KC_KP_8,  KC_KP_9,   KC_KP_PLUS,   KC_KP_ASTERISK,
        KC_BACKSPACE,            XXXXXXX,      XXXXXXX,      XXXXXXX,      KC_AUDIO_VOL_DOWN, KC_BRIGHTNESS_DOWN,      XXXXXXX,      KC_KP_4, KC_KP_5,  KC_KP_6,   KC_MINUS,     KC_KP_SLASH,
        KC_LEFT_SHIFT,           XXXXXXX,      XXXXXXX,      KC_CAPS_LOCK, KC_AUDIO_MUTE,     XXXXXXX,                 XXXXXXX,      KC_KP_1, KC_KP_2,  KC_KP_3,   KC_EQUAL,     KC_EQUAL,
        KC_LEFT_CTRL,            XXXXXXX,      KC_LALT,      KC_LEFT_GUI,  _______,           KC_SPACE,                KC_ENTER,     KC_KP_0, KC_COMMA, KC_KP_DOT, XXXXXXX,      KC_RIGHT_CTRL),
};

